# -*- coding: utf-8 -*-
'''
Created on 02 нояб. 2015 г.

@author: Даль
'''


from time import time, sleep
import datetime
import os

from PyQt5 import QtCore, QtGui, Qt
from PyQt5 import QtWidgets as qw
from PyQt5.Qt import QPoint, QStandardItem, QAbstractItemDelegate,\
    QStandardItemModel, QAbstractItemView, QModelIndex
from PyQt5.QtCore import pyqtSlot,  QCoreApplication, pyqtSignal, QObject,\
    QDir
from PyQt5.QtWidgets import (QDialog, QMainWindow, QItemDelegate,
                             QWidget, QFileSystemModel)

from BDStorage import (id_unique, bdGet, bdClose)
from PotokNTFSsecret import PotokNTFS
from frm.DirP_ui import Ui_fmFileOpis


# import random
def myCentrWin(self):
    # центрируем окно
    qr = self.frameGeometry()
    cp = qw.QDesktopWidget().availableGeometry().center()
    qr.moveCenter(cp)
    self.move(qr.topLeft())


class MyWin(QMainWindow, Ui_fmFileOpis):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setupUi(self)
#         bdOpen()
        self.proba()
        # Сигналы
        self.menu_file_Exit.triggered.connect(self.close)
        self.fmFileOpis.triggered.connect(self.menu_bd_initAll_Show)
#         self.fmQOrg_user.triggered.connect(self.orgAvtorShow)

    def closeEvent(self, event):
        #         if self.bdOpenBool:
        #             bd_close()
        #         event.accept() #else: #event.ignore()
        QCoreApplication.instance().quit()  # закрытие окна

    def menu_bd_initAll_Show(self, path_full=None):
        for i in range(500):
            self.lcdN.value += self.lcdN.value
            print(self.lcdN.value)
            sleep(1)
        exit()

        self.lcdN.setDigitCount(0)
        pt = PotokNTFS()
        if not path_full:
            path_full = os.getcwd()

        def dirRead(path_full):
            for nameF in os.listdir(path_full):
                # проиндексировать все файлы
                path_file = os.path.join(path_full, nameF)
    #             if os.path.isfile(path_file):
                pt_read = pt.potocRead(
                    path_file, pt.potok[pt.potokId])
                if not (pt_read and bdGet(pt_read)):  # == None:
                    pt.potocWrite(path_file,
                                  pt.potok[pt.potokId], id_unique())
                    self.lcdN.value += self.lcdN.value  # (self.lcdN.)
                if not os.path.isfile(path_file):
                    dirRead(path_full)

    def proba(self):
        model = QFileSystemModel()
        model.setRootPath(QDir.currentPath())

#         tree = new QTreeView(splitter);
        self.tree.setModel(model)
        self.tree.setRootIndex(model.index(QDir.currentPath()))
#         tree->setModel(model);
#         tree->setRootIndex(model->index(QDir::currentPath()))


def main():
    import sys
#     exit()
    app = qw.QApplication(sys.argv)
    win = MyWin()
    myCentrWin(win)
    win.show()
    sys.exit(app.exec_())
    bdClose()

if __name__ == "__main__":
    main()
