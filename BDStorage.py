# -*- coding: utf-8 -*-
'''
Created on 10 нояб. 2015 г.

@author: kandaurov_aa
'''

from random import choice
import os
import shelve

from PotokNTFSsecret import PotokNTFS
import string as st


pt = PotokNTFS()
bd = shelve.open(
    os.path.join('bd', 'bdStor'), flag='n', protocol=3, writeback=True)

# print('-1- не возможно вставить поле: ' + pole +
#                   ' в файл: ' + path + 'c значением: ' + znach)


def tst():
    #     print(gen_kod())
    initALL()
    bdClose()
    exit()
    # тестирование потоков
    #=========================================================================
    #     Работа с Альтернативными потоками в NTFS
    f = open("testP.txt:asa", 'w')
    f.write("Скрытый поток!")
    f.close()

    try:
        f = open("testP.txt:asaa", 'r')
    except FileNotFoundError as er:
        print("нет такого потока.")

    t = f.read()
    f.close()
    print(t)
#=========================================================================

# def bdOpen(bdBase=shelve):
#     bd = bdBase.open('bd\\bdStor', flag='n', protocol=3, writeback=True)


def bdClose():
    bd.close()


def bdClear():
    bd.clear()


def bdGet(potok_read):
    return bd.get(potok_read)


def gen_kod(dlinna=16):
    return ''.join([choice(st.ascii_letters + st.digits) for i in range(dlinna)])


def id_unique():
    # получить уникальный id для базы

    while 1:
        kod = gen_kod()
        if bd.get(kod) == None:
            return kod


def dirRead(path_full, par):
    for nameF in os.listdir(path_full):
        path = os.path.join(path_full, nameF)
        if os.path.isfile(path):
            if par == 'iALL':
                # проиндексировать все файлы
                pt_read = pt.potocRead(path, pt.potok[pt.potokId])
                if not (pt_read and bdGet(pt_read)):  # == None:
                    pt.potocWrite(path, pt.potok[pt.potokId], id_unique())
                    print('223 Проиндексирован: ', path)
#             if par == '':
                #
        else:
            dirRead(path, par)


def initALL(path=None):
    # Проиндексировать все файлы

    if not path:
        path = os.getcwd()
    dirRead(path, 'iALL')

if __name__ == '__main__':
    tst()
