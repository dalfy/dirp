# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DirP.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_fmFileOpis(object):
    def setupUi(self, fmFileOpis):
        fmFileOpis.setObjectName("fmFileOpis")
        fmFileOpis.resize(592, 636)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/File/src/Org.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        fmFileOpis.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(fmFileOpis)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.tree = QtWidgets.QTreeView(self.centralwidget)
        self.tree.setAnimated(True)
        self.tree.setObjectName("tree")
        self.gridLayout_2.addWidget(self.tree, 3, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.lcdN = QtWidgets.QLCDNumber(self.centralwidget)
        self.lcdN.setDigitCount(1)
        self.lcdN.setObjectName("lcdN")
        self.horizontalLayout.addWidget(self.lcdN)
        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        fmFileOpis.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(fmFileOpis)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 592, 21))
        self.menubar.setObjectName("menubar")
        self.menu_file = QtWidgets.QMenu(self.menubar)
        self.menu_file.setObjectName("menu_file")
        self.menu_bd = QtWidgets.QMenu(self.menubar)
        self.menu_bd.setObjectName("menu_bd")
        fmFileOpis.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(fmFileOpis)
        self.statusbar.setObjectName("statusbar")
        fmFileOpis.setStatusBar(self.statusbar)
        self.menu_file_open = QtWidgets.QAction(fmFileOpis)
        self.menu_file_open.setEnabled(False)
        self.menu_file_open.setObjectName("menu_file_open")
        self.menu_file_Exit = QtWidgets.QAction(fmFileOpis)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/File/src/power_black.png"), QtGui.QIcon.Active, QtGui.QIcon.Off)
        icon1.addPixmap(QtGui.QPixmap(":/File/src/power_blue.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.menu_file_Exit.setIcon(icon1)
        self.menu_file_Exit.setObjectName("menu_file_Exit")
        self.menu_bd_initAll = QtWidgets.QAction(fmFileOpis)
        self.menu_bd_initAll.setObjectName("menu_bd_initAll")
        self.menu_file.addAction(self.menu_file_open)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.menu_file_Exit)
        self.menu_bd.addAction(self.menu_bd_initAll)
        self.menubar.addAction(self.menu_file.menuAction())
        self.menubar.addAction(self.menu_bd.menuAction())

        self.retranslateUi(fmFileOpis)
        QtCore.QMetaObject.connectSlotsByName(fmFileOpis)

    def retranslateUi(self, fmFileOpis):
        _translate = QtCore.QCoreApplication.translate
        fmFileOpis.setWindowTitle(_translate("fmFileOpis", "MainWindow"))
        self.menu_file.setTitle(_translate("fmFileOpis", "Файл"))
        self.menu_bd.setTitle(_translate("fmFileOpis", "База"))
        self.menu_file_open.setText(_translate("fmFileOpis", "Открыть"))
        self.menu_file_Exit.setText(_translate("fmFileOpis", "Выход"))
        self.menu_bd_initAll.setText(_translate("fmFileOpis", "Проиндексировать все файлы"))

import DirP_rc
