'''
Created on 18 нояб. 2015 г.

@author: dal

[Описание]
Предназначен для чтения/записи альтрнативных потоков в NTFS, 
т.е. запись скрытых данных в описании файла.

[computer translation]
Designed for read / write altrnativnyh streams in NTFS, 
ct. recording hidden data file description.
'''


class PotokNTFS():
    # Работа с потоками в NTFS

    potok = ('dalfyId', 'dalfyName', 'dalfyOpis')
    potokId = 0
    potokName = 1
    potokOpis = 2
    # dalfyId - Идентификатор файла (если файл переименуют)
    # dalfyName - Внутреннее имя файла
    # dalfyOpis - Описание файла

    def potocRead(self, path, pole, default=None):
        # None - нет накого потока.
        try:
            f = open(path + ":" + pole, 'r')
            default = f.read()
            f.close()
        except FileNotFoundError:
            pass
        return default

    def potocWrite(self, path, pole, znach, default=True):
        try:
            f = open(path + ":" + pole, 'w')
            f.write(znach)
            f.close()
        except FileNotFoundError:
            default = False
        return default


def test():
    pt = PotokNTFS()
    path = 'tst.txt'
    pole = 'del'
    print('\n- Запись потока:')
    print(pt.potocWrite(path, pole, 'скрытое значение!'))
    print('\n- Чтение потока:')
    print(pt.potocRead(path, pole))
    print('\n- Чтение не существующего потока:')
    print(pt.potocRead(path, 'khbu6f'))
    print('- Чтение потока Name')
    a = pt.potok[pt.potokName]
    print(pt.potocRead(path, a))

if __name__ == '__main__':
    test()
