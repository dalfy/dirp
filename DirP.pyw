# -*- coding: utf-8 -*-
'''
Created on 02 нояб. 2015 г.

@author: Даль
'''


from time import time, sleep
import datetime
import os
import random

from PyQt5 import QtCore, QtGui, Qt
from PyQt5 import QtWidgets as qw
from PyQt5.Qt import QPoint, QStandardItem, QAbstractItemDelegate,\
    QStandardItemModel, QAbstractItemView, QModelIndex
from PyQt5.QtCore import pyqtSlot,  QCoreApplication, pyqtSignal, QObject,\
    QDir
from PyQt5.QtWidgets import (QDialog, QMainWindow, QItemDelegate,
                             QWidget, QFileSystemModel)

from BDStorage import (initALL, bdClose)
import frm.DirP_ui


def myCentrWin(self):
    # центрируем окно
    qr = self.frameGeometry()
    cp = qw.QDesktopWidget().availableGeometry().center()
    qr.moveCenter(cp)
    self.move(qr.topLeft())


class MyWin(QMainWindow, frm.DirP_ui.Ui_MainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setupUi(self)
#         bdOpen()
        self.proba()
        # Сигналы
        self.MExit.triggered.connect(self.close)
#         self.fmQOrg_user.triggered.connect(self.orgAvtorShow)

    def closeEvent(self, event):
        #         if self.bdOpenBool:
        #             bd_close()
        #         event.accept() #else: #event.ignore()
        QCoreApplication.instance().quit()  # закрытие окна

    def proba(self):
        model = QFileSystemModel()
        model.setRootPath(QDir.currentPath())

#         tree = new QTreeView(splitter);
        self.tree.setModel(model)
        self.tree.setRootIndex(model.index(QDir.currentPath()))
#         tree->setModel(model);
#         tree->setRootIndex(model->index(QDir::currentPath()))


def main():
    import sys

#     exit()
    app = qw.QApplication(sys.argv)
    win = MyWin()
    myCentrWin(win)
    win.show()
    sys.exit(app.exec_())
    bdClose()

if __name__ == "__main__":
    main()
